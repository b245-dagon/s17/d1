// console.log("Hello world");

// [SECTION] Functions

	// Functions is javascript are line/block of codes that tells out device/application to perform a specific task when called/invoked.
	// it prevents repeating line/blocks of codes that perform the same task /function.
	
	// Function Declarations
		// functions statement is the definitions of a function.

/*
	Syntax:
		function functionName(){
			code block (statement)
		}
	
	-function keyword - used to define a javascript function.
	-functionName- name of the function, which will be used to call/invoked the function.
	-function block({})- indicates the function body.
*/
	function printName(){
		console.log("My name is John");
	}
//  Function invocation
		// This run/execute the code block inside the function.

		printName();

		declaredFunction(); //we cannot invoked a function that we have not declared/defined yet.

// [SECTION] Function Declaration vs Function Expressions.
	// function can be created by using function keyword and adding a fuction name.
		// "saved for later used"
		// Declared function can be hoisted as log a function has been defined.
		function declaredFunction(){
			console.log("Hello world form declaredFunction()")
		}


		let variableFunction = function (){
			console.log("Hello Again!")
		};

		variableFunction();



		// We create also function expression with a named function.

		let funcExpression = function funcName(){
			console.log("Hello fro the other side");
		}

		// funcName(); // funcName()is not defined
		funcExpression();

		// Reassignment

		declaredFunction = function(){
			console.log ("Updated declaredFunction")
		}

		declaredFunction();

		funcExpression = function (){
			console.log("Updated funcExpression");

		}

		funcExpression();

		const contantFunc = function(){
			console.log("Initialized with const");
		}

		contantFunc();

		// This will result to reassignment error

		// contantFunc = function(){
		// 	console.log("Cannot be reassigned");
		// }

		// contantFunc();

		// [SECTION] Function Scoping

		/*
			scope is the accessibility (visibility) of variable.

			JavaScript Variable has 3 types of scope:
			1. glogbal scope
			2. local/block scope
			3. function scope

		*/

		// Global Scope

			// variable can be access any where from the program

		let globalVar = "Mr. WorldWide";

		// Local Scope
			// Variables declared inside a curly bracket ({})can only be accessed locally.
			// console.log(localVar);
		{
			// var localVar = "Armando Perez";
			let localVar = "Armando Perez";
			console.log(localVar);
		}

		console.log(globalVar);
		// console.log(localVar);

		// Function Scope
			// Each Functions creates a new scope
			// Variables defined inside a function are not accessible from outside the function.


		function showNames(){
			// Function Scope variable
			var functionVar = "Joe";
			const functionConst = "John";
			let functionLet = "Joey";

			console.log(functionVar);
			console.log(functionConst);
			console.log(functionLet);
		}

		showNames();

			// console.log(functionVar); // not accessible outside the function
			// console.log(functionConst);
			// console.log(functionLet);

		function myNewFunction(){
			let name = "Jane";

			function nestedFunc(){
				let nestedName= "John"
				console.log(name);
			}
			// console.log(nestedName);
			nestedFunc();
	}
			myNewFunction();

			// Global Scope variable

			let globalName = "Alex";

			function myNewFunction2(){
				let nameInside = "Renz";
				console.log(globalName);
			}
			myNewFunction2();

		// [SECTION] Using alert () and prompt ()

			// alert("Hello world");

			function showSampleAlert(){
				alert("Hello User");
			}
			
			// showSampleAlert();
			console.log("I will only log in the console when the alert is dismissed");

			// Noted on the use of alert():
		// prompt() allows us to show a small window at the top of the brower to gather user input

			/*
				Syntax : let/const variableName = prompt ("<dialogInString>");
			*/

			// let name = prompt("Enter your name: ");
			// let age = prompt("Enter your age: ");

			// console.log(typeof age);
			// console.log("Hello I am " +name+ " and  I am "+age+" years old.");

			// let sampleNullPrompt = prompt ("Do not Enter Anything");
			// console.log(sampleNullPrompt);


			function printWelcomeMessage(){
				let name = prompt("Enter your name:");
				console.log("Hello, " + name + "! Welcome to my page!");
			}

			printWelcomeMessage();





